<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="/WEB-INF/views/include.jsp" %>

<html>
  <head><title>Hello :: Spring Application 2</title></head>
  <body>
  	<form:form method="post" action="savePerson" modelAttribute="persons">
	    <h1>Hello - Spring Application</h1>
	    <p>Mi Lista:
	    	<c:forEach items="${listaUsuario}" var="usuario">
	    		<c:out value="${usuario.nombre}"/><br/>
	    	</c:forEach>
	    </p>
    </form:form>
  </body>
</html>