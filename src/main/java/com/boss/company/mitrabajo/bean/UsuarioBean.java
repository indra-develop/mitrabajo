package com.boss.company.mitrabajo.bean;

public class UsuarioBean {
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String tipDocumento;
	private Integer numDocumento;

	public UsuarioBean (String tipDocumento, Integer numDocumento, String nombre){
		this.tipDocumento = tipDocumento;
		this.numDocumento = numDocumento;
		this.nombre = nombre;
	}
	
	public UsuarioBean() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getTipDocumento() {
		return tipDocumento;
	}

	public void setTipDocumento(String tipDocumento) {
		this.tipDocumento = tipDocumento;
	}

	public Integer getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(Integer numDocumento) {
		this.numDocumento = numDocumento;
	}
}