package com.boss.company.mitrabajo.usuario.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.boss.company.mitrabajo.bean.UsuarioBean;
import com.boss.company.mitrabajo.usuario.dao.DUsuarioDao;

@Component
public class UsuarioDaoImpl implements DUsuarioDao {
	@Override
	public List<UsuarioBean> listarUsuario(){
		List<UsuarioBean> listaUsuario = new ArrayList<UsuarioBean>();
		UsuarioBean usuario1 = new UsuarioBean("DNI", 56984526, "Pepito");
		listaUsuario.add(usuario1);
		return listaUsuario;
	}
}