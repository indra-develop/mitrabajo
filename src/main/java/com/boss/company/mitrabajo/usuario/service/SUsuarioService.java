package com.boss.company.mitrabajo.usuario.service;

import java.util.List;

import com.boss.company.mitrabajo.bean.UsuarioBean;

public interface SUsuarioService {
	public List<UsuarioBean> listarUsuario();
}