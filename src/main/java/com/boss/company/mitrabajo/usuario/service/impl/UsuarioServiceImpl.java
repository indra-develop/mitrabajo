package com.boss.company.mitrabajo.usuario.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boss.company.mitrabajo.bean.UsuarioBean;
import com.boss.company.mitrabajo.usuario.dao.DUsuarioDao;
import com.boss.company.mitrabajo.usuario.service.SUsuarioService;

@Service
public class UsuarioServiceImpl implements SUsuarioService {
	@Autowired
	private DUsuarioDao usuarioDao;
	
	@Override
	public List<UsuarioBean> listarUsuario(){
		return usuarioDao.listarUsuario();
	}
}