package com.boss.company.mitrabajo.usuario.dao;

import java.util.List;

import com.boss.company.mitrabajo.bean.UsuarioBean;

public interface DUsuarioDao {
	public List<UsuarioBean> listarUsuario();
}