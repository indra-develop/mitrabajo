package com.boss.company.mitrabajo.principal.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.boss.company.mitrabajo.bean.UsuarioBean;
import com.boss.company.mitrabajo.usuario.service.SUsuarioService;

@Controller
public class CPrincipalController {
	private final static Logger LOGGER = Logger.getLogger(CPrincipalController.class.getCanonicalName());
	
	@Autowired
	private SUsuarioService usuarioService;
	
	@RequestMapping(value="/hello.htm")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		LOGGER.info("ENTRO");
		List<UsuarioBean> listaUsuario = usuarioService.listarUsuario();
		ModelAndView modelAndView = new ModelAndView("hello", "listaUsuario", listaUsuario);
		
		int aa = 0;
		try{
			int bb = 0/aa;
			System.out.println("-->bb:" + bb);
		}catch (Exception e){
			LOGGER.error("Error:", e);
		}
		
		return modelAndView;
	}
}